// https://doc.rust-lang.org/book/ch06-03-if-let.html

// traditional approach using match
// let mut count = 0;
// match coin {
//     Coin::Quarter(state) => println!("State quarter from {:?}!", state),
//     _ => count += 1,
// }

// using `if let`
// `if let` is used when only a single value needs to be matched and some other action is to be performed 
// when if the condition is not satisfied
let mut count = 0;
if let Coin::Quarter(state) = coin {
    println!("State quarter from {:?}!", state);
} else {
    count += 1;
}

fn main() {
    println!("Hello, world!");
}
