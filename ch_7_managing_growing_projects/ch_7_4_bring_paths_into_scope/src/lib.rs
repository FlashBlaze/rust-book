// https://doc.rust-lang.org/book/ch07-04-bringing-paths-into-scope-with-the-use-keyword.html

mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}


// We can bring a path into a scope once and then call the items in that path as if they’re local items with the use keyword.
use crate::front_of_house::hosting;

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
}