// https://doc.rust-lang.org/book/ch02-00-guessing-game-tutorial.html

use std::io;
use std::cmp::Ordering;
use rand::Rng;

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1, 101);

    loop {

        println!("Please input your guess.");

        // let is used to create variables. variables are immutable by default.
        // mut is used before variable name to make it mutable
        let mut guess = String::new();

        // like variables, references created by `&` are immutable, thus mut is used to make it mutable
        io::stdin().read_line(&mut guess)
            .expect("Failed to read line");

        // converting guess variable from String type to u32
        // instead of creating new variables, Rust allows us to shadow the previous value with a new one
        // let guess: u32 = guess.trim().parse()
        //     .expect("Please type a number!");

        // converting the above code to handle errors
        // `_` is a catchall value
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        }; // remember this semicolon

        println!("You guessed: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            },
        }
    }
}

// Note: You won’t just know which traits to use and which methods and functions to call from a crate. 
// Instructions for using a crate are in each crate’s documentation. 
// Another neat feature of Cargo is that you can run the cargo doc --open command, which will build 
// documentation provided by all of your dependencies locally and open it in your browser. 
// If you’re interested in other functionality in the rand crate, for example, run cargo doc --open and 
// click rand in the sidebar on the left.