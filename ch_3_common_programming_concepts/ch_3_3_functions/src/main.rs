fn main() {
    println!("Hello, world!");

    another_function();
    func_arg(5);
    func_wth_multiple_params(10, 20.0);

    let x = 5;
    let y = {
        let x = 3;
        // notice the lack of semicolon. expressions do not include semicolons.
        // adding a semicolon at the end will turn it into a statement which will return a value.
        x + 1 
    };

    println!("The value of y is: {} and value of x is: {}", y, x);

    let val = func_with_return(5);
    println!("The value of val is: {}", val);
}

fn another_function() {
    println!("Another function.");
}

fn func_arg(x: i32) {
    println!("The value of x is: {}", x);
}

fn func_wth_multiple_params(x: i32, y: f32) {
    println!("The value of x is: {}", x);
    println!("The value of y is: {}", y);
}

fn func_with_return(x: i32) -> i32 {
    // no semicolon since we want to return the value
    // adding a semicolon will give an error, since it will turn it into a statement
    x * x 

    // this works too
    // return x * x;

}