fn main() {
    let x = 3;

    // it is necessary that the value is bool. i.e. unlike js, `if x` won't automatically convert it to bool
    if x < 5 {
        println!("Less than 5");
    } else {
        println!("Greater than 5");
    }

    let number = 6;

    if number % 4 == 0 {
        println!("number is divisible by 4");
    } else if number % 3 == 0 {
        println!("number is divisible by 3");
    } else if number % 2 == 0 {
        println!("number is divisible by 2");
    } else {
        println!("number is not divisible by 4, 3, or 2");
    }

    let condition = true;

    // since if is an expression, we can use it like this
    // each arm of if should have same data type
    let number = if condition {
        5
    } else {
        6
    };

    println!("The value of number is: {}", number);

    // below loop will run indefinitely
    // loop {
    //     println!("Print!!!");
    // }

    
    let mut counter = 0;
    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2;
        }
    };
    println!("The result is {}", result);

    
    let mut number = 3;
    while number != 0 {
        println!("{}!", number);

        number -= 1;
    }
    println!("LIFTOFF!!!");


    let a = [10, 20, 30, 40, 50];
    let mut index = 0;
    while index < 5 {
        println!("the value is: {}", a[index]);

        index += 1;
    }


    // recommended way of using for loop
    let a = [10, 20, 30, 40, 50];
    for element in a.iter() {
        println!("the value is: {}", element);
    }


    // another example
    for number in (1..4).rev() {
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");
}
