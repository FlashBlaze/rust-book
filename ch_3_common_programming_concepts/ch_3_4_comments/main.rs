fn main() {
    // this is a single line comment
    /*
    This is 
    a
    multiline
    comment
    */
    let x = 5;
    println!("The value of x is: {}", x);
}