fn main() {
    let guess: i32 = "42".parse().expect("Not a number");
    println!("{}", guess);

    // floats
    let x = 2.0; // f64
    let y: f32 = 3.0; // f32

    // addition
    let sum = 5 + 10;

    // subtraction
    let difference = 95.5 - 4.3;

    // multiplication
    let product = 4 * 30;

    // division
    let quotient = 56.7 / 32.2;

    // remainder
    let remainder = 43 % 5;

    // booleans
    let t = true;
    let f: bool = false; // with explicit type annotation

    // char
    let c = 'z';
    let z = 'ℤ';
    let heart_eyed_cat = '😻';

    // tuple is of fixed length and cannot be changed later
    let tup: (i32, f64, u8) = (500, 6.4, 1);
    // destructuring to get values from tuple
    let (x, y, z) = tup;
    println!("The value of y is: {}", y);

    let five_hundred = tup.0;
    let six_point_four = tup.1;
    let one = tup.2;

    // unlike tuple, arrays must have th same type
    let arr_1 = [1, 2, 3, 4, 5];
    let arr_2: [i32; 5] = [1, 2, 3, 4, 5];
    // same as writing: let arr_3 = [3, 3, 3, 3, 3]
    let arr_3 = [3; 5];

    // accessing array elements
    let arr_4 = [1, 2, 3, 4, 5];
    let first = arr_4[0];
    let second = arr_4[0];
}
