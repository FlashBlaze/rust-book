fn main() {

    // since variables are immutable, mut keyword allows us to change value of variables
    let mut x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);


    let x = 5;
    
    // shadowing: this allows us to create a new variable of the same name which shadows the previous variable
    // by using let, we can perform a few transformations on a value but have the variable be immutable 
    // after those transformations have been completed.
    let x = x + 1;
    let x = x * 2;
    println!("The value of x is: {}", x);
}