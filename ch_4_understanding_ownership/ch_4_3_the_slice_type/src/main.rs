fn main() {

    // A string slice is a reference to part of a String, and it looks like this:
    let s = String::from("hello world");
    
    let hello = &s[0..5];
    let world = &s[6..11];

    let len = s.len();

    // both are same
    let slice = &s[0..2];
    let slice = &s[..2];

    // both are same
    let slice = &s[3..len];
    let slice = &s[3..];

    // both are same
    let slice = &s[0..len];
    let slice = &s[..];

    let s2 = first_word(&s);
    println!("{}", s2);
}

// this function returns first word of a sentence or the same word if single word is passed
fn first_word(s: &String) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}