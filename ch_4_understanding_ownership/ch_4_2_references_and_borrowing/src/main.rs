// https://doc.rust-lang.org/book/ch04-02-references-and-borrowing.html

fn main() {
    let s1 = String::from("hello");

    // These ampersands are references, and they allow you to refer to some value without taking ownership of it.
    // The &s1 syntax lets us create a reference that refers to the value of s1 but does not own it. 
    // Because it does not own it, the value it points to will not be dropped when the reference goes out of scope.
    let len = calculate_length(&s1);

    println!("The length of '{}' is {}.", s1, len);

    let mut s = String::from("hello");

    change(&mut s);
    println!("{}", s);

    // you can have only one mutable reference to a particular piece of data in a particular scope.
    // let r1 = &mut s;
    // let r2 = &mut s;
    // println!("{}{}", r1, r2);

    // multiple immutable are allowed since no one is able to change the value
    let r1 = &s;
    let r2 = &s;
    println!("{} {}", r1, r2);
    
    // this is not allowed. since it is changed from immutable to mutable and immutable is already referenced
    // let r3 = &mut s;
}

fn calculate_length(s: &String) -> usize { // s is a reference to a String
    s.len()
} // Here, s goes out of scope. But because it does not have ownership of what
  // it refers to, nothing happens.

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}