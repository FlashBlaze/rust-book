// https://doc.rust-lang.org/book/ch05-01-defining-structs.html

// initializing struct
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool
} // no semicolon

fn build_user(email: String, username: String) -> User {
    User {
        // using shorthand since the params and the field names match
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}

struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

fn main() {
    // creating an instance of struct
    let user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };

    // to change the values you need to make the entire instance mutable
    // rust does not allow for individual fields to be mutable
    let mut user2 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };

    user2.email = String::from("anotheremail@example.com");

    let user2 = User {
        email: String::from("another@example.com"),
        username: String::from("anotherusername567"),
        // using update syntax to use the same remaining values of user1 fields
        ..user1
    };

    let black = Color(0, 0, 0);
    let origin = Point(0, 0, 0);
}
