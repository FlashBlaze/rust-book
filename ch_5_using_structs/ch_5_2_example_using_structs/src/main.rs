// https://doc.rust-lang.org/book/ch05-02-example-structs.html

// this is required to display data in debug mode
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

fn main() {
    let rect1 = Rectangle { width: 30, height: 50 };

    println!(
        "The area of the rectangle is {} square pixels.",
        area(&rect1)
    );

    // `:?` allows us to display data in debug mode1
    println!("{:?}", rect1);
    
    // in larger structs you should use `:#?` which makes it easier to read
    println!("{:#?}", rect1);

}

// `&` is used so that `main` retains the ownership of rect1 and can still use it further if needed
fn area(rectangle: &Rectangle) -> u32 {
    rectangle.width * rectangle.height
}